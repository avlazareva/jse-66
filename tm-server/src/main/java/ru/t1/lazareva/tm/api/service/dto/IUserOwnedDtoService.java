package ru.t1.lazareva.tm.api.service.dto;

import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.dto.model.AbstractUserOwnedModelDto;
import ru.t1.lazareva.tm.enumerated.Sort;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedDtoService<M extends AbstractUserOwnedModelDto> extends IDtoService<M> {

    M add(@Nullable String userId, M model) throws Exception;

    void clear(@Nullable String userId) throws Exception;

    boolean existsById(@Nullable String userId, @Nullable String id) throws Exception;

    boolean existsByIndex(@Nullable String userId, @Nullable Integer index) throws Exception;

    @Nullable List<M> findAll(@Nullable String userId) throws Exception;

    @Nullable List<M> findAll(@Nullable String userId, @Nullable Comparator comparator) throws Exception;

    @Nullable
    List<M> findAll(@Nullable String userId, @Nullable Sort sort) throws Exception;

    M findOneById(@Nullable String userId, @Nullable String id) throws Exception;

    M findOneByIndex(@Nullable String userId, @Nullable Integer index) throws Exception;

    void remove(@Nullable String userId, M model) throws Exception;

    void removeById(@Nullable String userId, @Nullable String id) throws Exception;

    void removeByIndex(@Nullable String userId, @Nullable Integer index) throws Exception;

    M update(@Nullable String userId, M model) throws Exception;
}
