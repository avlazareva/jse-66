package ru.t1.lazareva.tm.service;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import ru.t1.lazareva.tm.api.service.dto.ISessionDtoService;
import ru.t1.lazareva.tm.api.service.dto.IUserDtoService;
import ru.t1.lazareva.tm.dto.model.SessionDto;
import ru.t1.lazareva.tm.exception.entity.EntityNotFoundException;
import ru.t1.lazareva.tm.exception.field.IdEmptyException;
import ru.t1.lazareva.tm.exception.field.UserIdEmptyException;
import ru.t1.lazareva.tm.marker.UnitCategory;
import ru.t1.lazareva.tm.migration.AbstractSchemeTest;

import java.util.Collection;

import static ru.t1.lazareva.tm.constant.ProjectTestData.USER_1;
import static ru.t1.lazareva.tm.constant.ProjectTestData.USER_PROJECT1;
import static ru.t1.lazareva.tm.constant.SessionTestData.*;

@Category(UnitCategory.class)
public final class SessionServiceTest extends AbstractSchemeTest {

    @NotNull
    @Autowired
    private ISessionDtoService service;

    @NotNull
    @Autowired
    private IUserDtoService userService;

    @BeforeClass
    public static void changeSchema() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
    }

    @Before
    public void before() throws Exception {
        userService.add(USER_1);
        userService.add(USER_2);
        service.add(USER_1.getId(), USER_1_SESSION);
        service.add(USER_2.getId(), USER_2_SESSION);
    }

    @After
    public void after() throws Exception {
        service.clear();
        userService.clear();
    }

    @Test
    public void add() throws Exception {
        service.clear();
        Assert.assertThrows(EntityNotFoundException.class, () -> {
            service.add(NULL_SESSION);
        });
        service.clear();
        Assert.assertNotNull(service.add(USER_1_SESSION));
        @Nullable final SessionDto session = service.findOneById(USER_1_SESSION.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(USER_1_SESSION.getId(), session.getId());
    }

    @Test
    public void addByUserId() throws Exception {
        Assert.assertThrows(EntityNotFoundException.class, () -> {
            service.add(USER_1.getId(), NULL_SESSION);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.add(null, USER_1_SESSION);
        });
        service.clear();
        Assert.assertNotNull(service.add(USER_1.getId(), USER_1_SESSION));
        @Nullable final SessionDto session = service.findOneById(USER_1.getId(), USER_1_SESSION.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(USER_1_SESSION.getId(), session.getId());
    }

    @Test
    public void findAll() {
        @NotNull final Collection<SessionDto> sessions = service.findAll();
        Assert.assertNotNull(sessions);
    }

    @Test
    public void existsById() throws Exception {
        Assert.assertFalse(service.existsById(""));
        Assert.assertFalse(service.existsById(null));
        Assert.assertFalse(service.existsById(NON_EXISTING_SESSION_ID));
        Assert.assertTrue(service.existsById(USER_1_SESSION.getId()));
    }

    @Test
    public void existsByIdByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.existsById(null, NON_EXISTING_SESSION_ID);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.existsById("", NON_EXISTING_SESSION_ID);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.existsById(USER_1.getId(), null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.existsById(USER_1.getId(), "");
        });
        Assert.assertFalse(service.existsById(USER_1.getId(), NON_EXISTING_SESSION_ID));
        Assert.assertTrue(service.existsById(USER_1.getId(), USER_1_SESSION.getId()));
    }

    @Test
    public void findOneById() throws Exception {
        Assert.assertThrows(EntityNotFoundException.class, () -> {
            service.findOneById(NON_EXISTING_SESSION_ID);
        });
        @Nullable final SessionDto session = service.findOneById(USER_1_SESSION.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(USER_1_SESSION.getId(), session.getId());
    }

    @Test
    public void findOneByIdByUserId() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.findOneById(USER_1.getId(), null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.findOneById(USER_1.getId(), "");
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.existsById(null, USER_1_SESSION.getId());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.existsById("", USER_1_SESSION.getId());
        });
        Assert.assertNull(service.findOneById(USER_1.getId(), NON_EXISTING_SESSION_ID));
        @Nullable final SessionDto session = service.findOneById(USER_1.getId(), USER_1_SESSION.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(USER_1_SESSION.getId(), session.getId());
    }

    @Test
    public void clear() throws Exception {
        service.clear();
        Assert.assertEquals(0, service.getSize());
    }

    @Test
    public void clearByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.clear(null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.clear("");
        });
        service.clear();
        service.add(USER_1.getId(), USER_1_SESSION);
        service.clear(USER_1.getId());
        Assert.assertEquals(0, service.getSize());
    }

    @Test
    public void remove() throws Exception {
        Assert.assertThrows(EntityNotFoundException.class, () -> {
            service.remove(null);
        });
        service.clear();
        @Nullable final SessionDto createdSession = service.add(USER_1_SESSION);
        service.remove(createdSession);
        Assert.assertEquals(0, service.getSize());
    }

    @Test
    public void removeByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.remove(null, null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.remove("", null);
        });
        Assert.assertThrows(EntityNotFoundException.class, () -> {
            service.remove(USER_1.getId(), null);
        });
        service.clear();
        @Nullable final SessionDto createdSession = service.add(USER_1_SESSION);
        service.remove(USER_1.getId(), createdSession);
        Assert.assertNull(service.findOneById(USER_1.getId(), USER_PROJECT1.getId()));
    }

    @Test
    public void removeByIdByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeById(null, null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeById("", null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById(USER_1.getId(), null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById(USER_1.getId(), "");
        });
        service.removeById(USER_1.getId(), USER_1_SESSION.getId());
        Assert.assertNull(service.findOneById(USER_1.getId(), USER_1_SESSION.getId()));
        @Nullable final SessionDto createdSession = service.add(USER_1_SESSION);
        service.removeById(USER_1.getId(), createdSession.getId());
        Assert.assertNull(service.findOneById(USER_1.getId(), USER_1_SESSION.getId()));
    }

    @Test
    public void getSize() throws Exception {
        service.clear();
        service.add(USER_1_SESSION);
        Assert.assertEquals(1, service.getSize());
    }

}