package ru.t1.lazareva.tm.service;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import ru.t1.lazareva.tm.api.service.IPropertyService;
import ru.t1.lazareva.tm.api.service.dto.IUserDtoService;
import ru.t1.lazareva.tm.dto.model.UserDto;
import ru.t1.lazareva.tm.enumerated.Role;
import ru.t1.lazareva.tm.exception.entity.EntityNotFoundException;
import ru.t1.lazareva.tm.exception.field.EmailEmptyException;
import ru.t1.lazareva.tm.exception.field.IdEmptyException;
import ru.t1.lazareva.tm.exception.field.LoginEmptyException;
import ru.t1.lazareva.tm.exception.field.PasswordEmptyException;
import ru.t1.lazareva.tm.exception.user.RoleEmptyException;
import ru.t1.lazareva.tm.marker.UnitCategory;
import ru.t1.lazareva.tm.migration.AbstractSchemeTest;
import ru.t1.lazareva.tm.util.HashUtil;

import java.util.Collection;

import static ru.t1.lazareva.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class UserServiceTest extends AbstractSchemeTest {

    @NotNull
    @Autowired
    private IUserDtoService service;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @BeforeClass
    public static void changeSchema() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
    }

    @Before
    public void before() throws Exception {
        service.add(USER_1);
        service.add(USER_2);
    }

    @After
    public void after() throws Exception {
        service.clear();
    }

    @Test
    public void add() throws Exception {
        service.clear();
        @Nullable final UserDto user = service.add(USER_1);
        Assert.assertNotNull(user);
        @Nullable final String userId = service.findOneById(user.getId()).getId();
        Assert.assertNotNull(userId);
        Assert.assertEquals(userId, USER_1.getId());
    }

    @Test
    public void findAll() {
        @NotNull final Collection<UserDto> userFindAllNoEmpty = service.findAll();
        Assert.assertNotNull(userFindAllNoEmpty);
    }

    @Test
    public void existsById() throws Exception {
        Assert.assertFalse(service.existsById(""));
        Assert.assertFalse(service.existsById(null));
        Assert.assertFalse(service.existsById(NON_EXISTING_USER_ID));
        Assert.assertTrue(service.existsById(USER_1.getId()));
    }

    @Test
    public void findOneById() throws Exception {
        Assert.assertThrows(EntityNotFoundException.class, () -> {
            service.findOneById(NON_EXISTING_USER_ID);
        });
        @Nullable final UserDto user = service.findOneById(USER_1.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(USER_1.getId(), user.getId());
    }

    @Test
    public void clear() throws Exception {
        service.clear();
        Assert.assertEquals(0, service.getSize());
    }

    @Test
    public void removeById() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById(null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById("");
        });
        Assert.assertThrows(EntityNotFoundException.class, () -> {
            service.removeById(NON_EXISTING_USER_ID);
        });
        service.clear();
        @Nullable final UserDto createdUser = service.add(USER_1);
        service.remove(createdUser);
        Assert.assertThrows(EntityNotFoundException.class, () -> service.findOneById(USER_1.getId()));
    }

    @Test
    public void getSize() throws Exception {
        service.clear();
        service.add(USER_1);
        Assert.assertEquals(1, service.getSize());
    }

    @Test
    public void create() throws Exception {
        service.clear();
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.create(null, ADMIN_TEST_PASSWORD);
        });
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.create("", ADMIN_TEST_PASSWORD);
        });
        Assert.assertThrows(PasswordEmptyException.class, () -> {
            service.create(ADMIN_TEST_LOGIN, null);
        });
        Assert.assertThrows(PasswordEmptyException.class, () -> {
            service.create(ADMIN_TEST_LOGIN, "");
        });
        service.clear();
        @NotNull final UserDto user = service.create(ADMIN_TEST_LOGIN, ADMIN_TEST_PASSWORD);
        Assert.assertNotNull(user);
        Assert.assertEquals(ADMIN_TEST_LOGIN, user.getLogin());
        Assert.assertEquals(HashUtil.salt(propertyService, ADMIN_TEST_PASSWORD), user.getPasswordHash());
    }

    @Test
    public void createWithEmail() throws Exception {
        service.clear();
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.create(null, ADMIN_TEST_PASSWORD, ADMIN_TEST_EMAIL);
        });
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.create("", ADMIN_TEST_PASSWORD, ADMIN_TEST_EMAIL);
        });
        Assert.assertThrows(PasswordEmptyException.class, () -> {
            service.create(ADMIN_TEST_LOGIN, null, ADMIN_TEST_EMAIL);
        });
        Assert.assertThrows(PasswordEmptyException.class, () -> {
            service.create(ADMIN_TEST_LOGIN, "", ADMIN_TEST_EMAIL);
        });
        @NotNull final UserDto user = service.create(ADMIN_TEST_LOGIN, ADMIN_TEST_PASSWORD, ADMIN_TEST_EMAIL);
        Assert.assertNotNull(user);
        Assert.assertEquals(ADMIN_TEST_LOGIN, user.getLogin());
        Assert.assertEquals(HashUtil.salt(propertyService, ADMIN_TEST_PASSWORD), user.getPasswordHash());
        Assert.assertEquals(ADMIN_TEST_EMAIL, user.getEmail());
    }

    @Test
    public void createWithRole() throws Exception {
        service.clear();
        @NotNull final Role role = Role.ADMIN;
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.create(null, ADMIN_TEST_PASSWORD, role);
        });
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.create("", ADMIN_TEST_PASSWORD, role);
        });
        Assert.assertThrows(PasswordEmptyException.class, () -> {
            service.create(ADMIN_TEST_LOGIN, null, role);
        });
        Assert.assertThrows(PasswordEmptyException.class, () -> {
            service.create(ADMIN_TEST_LOGIN, "", role);
        });
        Assert.assertThrows(RoleEmptyException.class, () -> {
            @NotNull final Role nullRole = null;
            service.create(ADMIN_TEST_LOGIN, ADMIN_TEST_PASSWORD, nullRole);
        });
        @NotNull final UserDto user = service.create(ADMIN_TEST_LOGIN, ADMIN_TEST_PASSWORD, Role.ADMIN);
        Assert.assertNotNull(user);
        Assert.assertEquals(ADMIN_TEST_LOGIN, user.getLogin());
        Assert.assertEquals(HashUtil.salt(propertyService, ADMIN_TEST_PASSWORD), user.getPasswordHash());
        Assert.assertEquals(Role.ADMIN, user.getRole());
    }

    @Test
    public void findByLogin() throws Exception {
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.findByLogin(null);
        });
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.findByLogin("");
        });
        service.clear();
        @NotNull final UserDto userCreated = service.create(USER_TEST_LOGIN, USER_TEST_PASSWORD, USER_TEST_EMAIL);
        @Nullable final UserDto userLogin = service.findByLogin(userCreated.getLogin());
        Assert.assertNotNull(userLogin);
        Assert.assertEquals(userCreated.getEmail(), userLogin.getEmail());
    }

    @Test
    public void findByEmail() throws Exception {
        Assert.assertThrows(EmailEmptyException.class, () -> {
            service.findByEmail(null);
        });
        Assert.assertThrows(EmailEmptyException.class, () -> {
            service.findByEmail("");
        });
        service.clear();
        @NotNull final UserDto userCreated = service.create(USER_TEST_LOGIN, USER_TEST_PASSWORD, USER_TEST_EMAIL);
        @Nullable final UserDto userEmail = service.findByEmail(userCreated.getEmail());
        Assert.assertNotNull(userEmail);
        Assert.assertEquals(userCreated.getEmail(), userEmail.getEmail());
    }

    @Test
    public void remove() throws Exception {
        Assert.assertThrows(EntityNotFoundException.class, () -> {
            service.remove(null);
        });
        service.clear();
        @Nullable final UserDto createdUser = service.add(USER_1);
        service.remove(createdUser);
        Assert.assertThrows(EntityNotFoundException.class, () -> service.findOneById(USER_1.getId()));
    }

    @Test
    public void removeByLogin() throws Exception {
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.removeByLogin(null);
        });
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.removeByLogin("");
        });
        Assert.assertThrows(EntityNotFoundException.class, () -> {
            service.removeByLogin(NON_EXISTING_USER_ID);
        });
        service.clear();
        service.add(USER_1);
        service.removeByLogin(USER_TEST_LOGIN);
        Assert.assertThrows(EntityNotFoundException.class, () -> service.findOneById(USER_1.getId()));
    }

    @Test
    public void setPassword() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.setPassword(null, ADMIN_TEST_PASSWORD);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.setPassword("", ADMIN_TEST_PASSWORD);
        });
        Assert.assertThrows(PasswordEmptyException.class, () -> {
            service.setPassword(USER_1.getId(), null);
        });
        Assert.assertThrows(PasswordEmptyException.class, () -> {
            service.setPassword(USER_1.getId(), "");
        });
        Assert.assertThrows(EntityNotFoundException.class, () -> {
            service.setPassword(NON_EXISTING_USER_ID, ADMIN_TEST_PASSWORD);
        });
        service.clear();
        @NotNull final UserDto user = service.create(ADMIN_TEST_LOGIN, ADMIN_TEST_PASSWORD);
        @Nullable final UserDto userUpdated = service.setPassword(user.getId(), USER_TEST_PASSWORD);
        Assert.assertNotNull(userUpdated);
        Assert.assertEquals(userUpdated.getId(), user.getId());
        Assert.assertEquals(HashUtil.salt(propertyService, USER_TEST_PASSWORD), userUpdated.getPasswordHash());
    }

    @Test
    public void updateUser() throws Exception {
        @NotNull final String firstName = "User_first_name";
        @NotNull final String lastName = "User_last_name";
        @NotNull final String middleName = "User_middle_name";
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.update(null, firstName, lastName, middleName);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.update("", firstName, lastName, middleName);
        });
        @NotNull final UserDto user = service.update(USER_1.getId(), firstName, lastName, middleName);
        Assert.assertEquals(firstName, user.getFirstName());
        Assert.assertEquals(lastName, user.getLastName());
        Assert.assertEquals(middleName, user.getMiddleName());
    }

    @Test
    public void isLoginExists() throws Exception {
        Assert.assertFalse(service.isLoginExists(null));
        Assert.assertFalse(service.isLoginExists(""));
        Assert.assertTrue(service.isLoginExists(USER_TEST_LOGIN));
    }

    @Test
    public void isEmailExists() throws Exception {
        Assert.assertFalse(service.isEmailExists(null));
        Assert.assertFalse(service.isEmailExists(""));
        Assert.assertTrue(service.isEmailExists(USER_TEST_EMAIL));
    }

    @Test
    public void lockUserByLogin() throws Exception {
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.lockUserByLogin(null);
        });
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.lockUserByLogin("");
        });
        Assert.assertThrows(EntityNotFoundException.class, () -> {
            service.lockUserByLogin(NON_EXISTING_USER_LOGIN);
        });
        service.lockUserByLogin(USER_1.getLogin());
        @Nullable final UserDto user = service.findOneById(USER_1.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(user.getId(), USER_1.getId());
        Assert.assertTrue(user.getLocked());
    }

    @Test
    public void unlockUserByLogin() throws Exception {
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.unlockUserByLogin(null);
        });
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.unlockUserByLogin("");
        });
        Assert.assertThrows(EntityNotFoundException.class, () -> {
            service.unlockUserByLogin(NON_EXISTING_USER_ID);
        });
        service.clear();
        service.add(USER_1);
        service.lockUserByLogin(USER_TEST_LOGIN);
        service.unlockUserByLogin(USER_TEST_LOGIN);
        Assert.assertFalse(USER_1.getLocked());
    }

}
